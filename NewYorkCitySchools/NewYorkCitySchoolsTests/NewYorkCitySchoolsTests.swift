//
//  NewYorkCitySchoolsTests.swift
//  NewYorkCitySchoolsTests
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import XCTest
import Moya
import SwiftyJSON

@testable import NewYorkCitySchools

class NewYorkCitySchoolsTests: XCTestCase {

    let stubbingProvider = MoyaProvider<NYCSchoolDataService>(stubClosure: MoyaProvider.immediatelyStub)

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetSchools() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectation = self.expectation(description:  "test request")
        _ = stubbingProvider.request(.getSchools) { result in
            // pass or fail depending on your test needs
            switch result {
            case let .success(moyaResponse):
                do {
                    
                    try _ = moyaResponse.filterSuccessfulStatusCodes()
                    
                    let data = try moyaResponse.mapJSON()
                    let data2 = JSON(data)
                    
                    XCTAssertEqual(data2.count , 3)
                    
                }
                catch {
                    // NOOP
                    // Swallow the error as we have a seed file.
                    // If this was necssary send a notification
                    XCTFail("didn't receive test  200")
                    
                }
            // do something in your app
            case let .failure(error):
                // TODO: handle the error == best. comment. ever.
                print("\(error)")
                // NOOP
                // Swallow the error as we have a seed file.
                // If this was necssary send a notification
                
                
            }
            
            
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 5.0, handler: nil)
        
    }
    
    func testGetSchoolScores() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectation = self.expectation(description:  "test request")
        _ = stubbingProvider.request(.getSchoolScores) { result in
            // pass or fail depending on your test needs
            switch result {
            case let .success(moyaResponse):
                do {
                    
                    try _ = moyaResponse.filterSuccessfulStatusCodes()
                    
                    let data = try moyaResponse.mapJSON()
                    let data2 = JSON(data)
                    
                    XCTAssertEqual(data2.count , 2)
                    
                }
                catch {
                    // NOOP
                    // Swallow the error as we have a seed file.
                    // If this was necssary send a notification
                    XCTFail("didn't receive test  200")
                    
                }
            // do something in your app
            case let .failure(error):
                // TODO: handle the error == best. comment. ever.
                print("\(error)")
                // NOOP
                // Swallow the error as we have a seed file.
                // If this was necssary send a notification
                
                
            }
            
            
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 5.0, handler: nil)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
