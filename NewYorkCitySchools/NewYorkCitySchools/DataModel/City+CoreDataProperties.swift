//
//  City+CoreDataProperties.swift
//  
//
//  Created by James Cicenia on 10/21/18.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var avg_num_of_sat_test_takers: Int16
    @NSManaged public var avg_sat_critical_reading_avg_score: Int16
    @NSManaged public var avg_sat_math_avg_score: Int16
    @NSManaged public var avg_sat_writing_avg_score: Int16
    @NSManaged public var name: String?

}
