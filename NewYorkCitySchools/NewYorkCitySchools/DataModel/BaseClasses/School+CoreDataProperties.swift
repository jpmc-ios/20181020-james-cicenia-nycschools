//
//  School+CoreDataProperties.swift
//  
//
//  Created by James Cicenia on 10/20/18.
//
//

import Foundation
import CoreData


extension School {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<School> {
        return NSFetchRequest<School>(entityName: "School")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var school_name: String?
    @NSManaged public var sectionKey: String?
    @NSManaged public var total_students: String?
    @NSManaged public var website: String?
    @NSManaged public var phone_number: String?
    @NSManaged public var location: String?
    @NSManaged public var borough: String?
    @NSManaged public var score: SchoolScore?

}
