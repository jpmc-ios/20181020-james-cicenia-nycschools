//
//  AppDelegate.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import UIKit
import Sync

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let modelName = "NYCModel"
    let storeName = "NYCData-1.0"
    
    var dataStack:DataStack!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        //Setup the datasource
        dataStack = DataStack(modelName: modelName, bundle: Bundle.main, storeType: .sqLite, storeName: storeName)

        //Copy in the SEED database
        copyBundledSQLiteDB(source:"SEED", destination:storeName)
        print("copying over NYC-SEED to \(storeName)")

        //Sync the seed database in the backgroud for possible changes
        School.syncNYCData(dataStack: dataStack)
        
        print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String)
        
        //Set Appearance
        UITabBar.appearance().barTintColor = UIColor(named: "brand-color")
        UITabBar.appearance().tintColor = UIColor(named: "brand-color")
        UINavigationBar.appearance().barTintColor = UIColor(named: "brand-color")
        UINavigationBar.appearance().tintColor = UIColor(named: "brand-color")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

