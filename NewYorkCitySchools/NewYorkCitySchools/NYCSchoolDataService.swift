//
//  NYCSchoolDataService.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import Foundation



import Foundation
import SwiftyJSON
import Alamofire
import RxSwift
import Moya



/*
 * I chose to use the Moya Framework as it provides clarity  and disoipline to
 * developing rest client services
 */


enum NYCSchoolDataService{
    case getSchools
    case getSchoolScores
}



extension NYCSchoolDataService: TargetType {
    
    var baseURL: URL { return URL(string: "https://data.cityofnewyork.us/")! }

    var appToken:String {
        return   "RSXaytbEsBzIcndTmI9i6masJ"
    }
    
    
    var sampleData: Data {
        
        switch self {
        case .getSchools:
            
            return "[{ \"academicopportunities1\" : \"Early college experience through classes taught in collaboration with the faculty of St. JohnÂ’s University (SJU) at the Queens Campus\", \"academicopportunities2\" : \"CAD Design and 3D Printing in conjunction with Parsons/The New School for Design, Chess and Comparative Anatomy & Physiology\", \"academicopportunities3\" : \"Music program includes Latin Jazz, Rock, and String Ensembles\", \"academicopportunities4\" : \"Internships with the Queens Library, Chapin Home for the Aged, Grace Episcopal ChurchÂ’s Food Pantry as well as other community-based organizations\", \"academicopportunities5\" : \"Advisory and peer support systems; Kaplan K-12 SAT prep classes and a four-year sequence of college trips and college counseling services\", \"addtl_info1\" : \"Internships; Uniform\", \"admissionspriority11\" : \"Priority to Queens students or residents who attend an information session\", \"admissionspriority21\" : \"Then to New York City residents who attend an information session\", \"admissionspriority31\" : \"Then to Queens students or residents\", \"admissionspriority41\" : \"Then to New York City residents\", \"advancedplacement_courses\" : \"AP Biology, AP English, AP US History\", \"attendance_rate\" : \"0.829999983\", \"bbl\" : \"4098580100\", \"bin\" : \"4448806\", \"boro\" : \"Q\", \"borough\" : \"QUEENS   \", \"building_code\" : \"Q470\", \"bus\" : \"Q1, Q17, Q2, Q25, Q3, Q30, Q31, Q34, Q36, Q43, Q6, Q65, Q76, Q77, Q8, X68\", \"census_tract\" : \"452\", \"city\" : \"Jamaica\", \"code1\" : \"Q33A\", \"college_career_rate\" : \"0.547999978\", \"community_board\" : \"8\", \"council_district\" : \"24\", \"dbn\" : \"28Q328\", \"ell_programs\" : \"English as a New Language\", \"end_time\" : \"3:45pm\", \"extracurricular_activities\" : \"Student Government, Latin American and Caribbean Student Union (LACSU), Student Youth Fellowship Association (SYFA), Rock and Latin Jazz Bands, String Ensemble, Step & Dance Team, Computer Coding (C++, JavaScript, Python), Chess Club and Class, 3D Modeling with 3D Printers and Laser Cutting, Service Learning\", \"fax_number\" : \"718-558-9807\", \"finalgrades\" : \"9-12\", \"grade9geapplicants1\" : \"361\", \"grade9geapplicantsperseat1\" : \"4\", \"grade9gefilledflag1\" : \"N\", \"grade9swdapplicants1\" : \"71\", \"grade9swdapplicantsperseat1\" : \"4\", \"grade9swdfilledflag1\" : \"N\", \"grades2018\" : \"9-12\", \"graduation_rate\" : \"0.842999995\", \"interest1\" : \"Humanities & Interdisciplinary\", \"language_classes\" : \"French, Spanish\", \"latitude\" : \"40.71455\", \"location\" : \"167-01 Gothic Drive, Jamaica NY 11432 (40.714551, -73.798658)\", \"longitude\" : \"-73.7987\", \"method1\" : \"Limited Unscreened\", \"neighborhood\" : \"Briarwood-Jamaica Hills\", \"nta\" : \"Briarwood-Jamaica Hills                                                    \", \"offer_rate1\" : \"Â—61% of offers went to this group\", \"overview_paragraph\" : \"High School for Community Leadership (HSCL) encourages the development of well-rounded individuals who are prepared for university success and capable of exercising leadership in the global community.\", \"pct_stu_enough_variety\" : \"0.879999995\", \"pct_stu_safe\" : \"0.910000026\", \"phone_number\" : \"718-558-9801\", \"primary_address_line_1\" : \"167-01 Gothic Drive\", \"program1\" : \"High School for Community Leadership\", \"psal_sports_boys\" : \"Baseball, Basketball, Bowling, Fencing, Football, Lacrosse, Outdoor Track, Soccer, Swimming, Tennis, Volleyball, Wrestling\", \"psal_sports_coed\" : \"Cricket, Double Dutch, Golf, Stunt\", \"psal_sports_girls\" : \"Basketball, Bowling, Cross Country, Flag Football, Indoor Track, Outdoor Track, Soccer, Softball, Swimming, Volleyball\", \"school_email\" : \"Cborrero@schools.nyc.gov\", \"school_name\" : \"High School for Community Leadership\", \"seats101\" : \"No\", \"seats9ge1\" : \"92\", \"seats9swd1\" : \"16\", \"shared_space\" : \"Yes\", \"start_time\" : \"9am\", \"state_code\" : \"NY\", \"subway\" : \"F to 169th St\", \"total_students\" : \"444\", \"website\" : \"www.nychscl.org\", \"zip\" : \"11432\" }, { \"academicopportunities1\" : \"CTE program(s) in: Health Science\", \"academicopportunities2\" : \"CTE Programs in Health Careers; Literacy Focus (Readers and Writers Seminar); College and Career Advisory\", \"academicopportunities3\" : \"CUNY College Now courses in Health Science; APEX Learning (all subjects)\", \"admissionspriority11\" : \"Priority to Brooklyn students or residents who attend an information session\", \"admissionspriority21\" : \"Then to New York City residents who attend an information session\", \"admissionspriority31\" : \"Then to Brooklyn students or residents\", \"admissionspriority41\" : \"Then to New York City residents\", \"attendance_rate\" : \"0.839999974\", \"bbl\" : \"3012160046\", \"bin\" : \"3030553\", \"boro\" : \"K\", \"borough\" : \"BROOKLYN \", \"building_code\" : \"K625\", \"bus\" : \"B15, B25, B26, B43, B44, B45, B46, B65\", \"campus_name\" : \"Paul Robeson Educational Campus\", \"census_tract\" : \"311\", \"city\" : \"Brooklyn\", \"code1\" : \"L64A\", \"college_career_rate\" : \"0.483999997\", \"community_board\" : \"8\", \"council_district\" : \"36\", \"dbn\" : \"17K751\", \"ell_programs\" : \"English as a New Language\", \"end_time\" : \"2:45pm\", \"extracurricular_activities\" : \"After-School Tutoring, Community Service, Creative Arts, Homework Center, Instrumental Music, Newspaper, Peer Mediation, Peer Tutoring, Performing Arts, Student Government\", \"fax_number\" : \"718-773-0648\", \"finalgrades\" : \"9-12\", \"grade9geapplicants1\" : \"524\", \"grade9geapplicantsperseat1\" : \"6\", \"grade9gefilledflag1\" : \"N\", \"grade9swdapplicants1\" : \"85\", \"grade9swdapplicantsperseat1\" : \"5\", \"grade9swdfilledflag1\" : \"N\", \"grades2018\" : \"9-12\", \"graduation_rate\" : \"0.727999985\", \"interest1\" : \"Health Professions\", \"language_classes\" : \"French, Spanish\", \"latitude\" : \"40.67591\", \"location\" : \"150 Albany Avenue, Brooklyn NY 11213 (40.675905, -73.939282)\", \"longitude\" : \"-73.9393\", \"method1\" : \"Limited Unscreened\", \"neighborhood\" : \"Crown Heights North\", \"nta\" : \"Crown Heights North                                                        \", \"offer_rate1\" : \"Â—56% of offers went to this group\", \"overview_paragraph\" : \"The mission of the Academy for Health Careers is to ensure that every student develops a strong foundation in health studies, science, leadership, communication, and other competencies that are essential for careers in the healthcare services industry. Our goal is to have students who will be college- and career-ready upon graduation, prepared to participate in the global marketplace as skilled problem solvers, critical thinkers, and self-sustaining citizens.\", \"pct_stu_enough_variety\" : \"0.529999971\", \"pct_stu_safe\" : \"0.769999981\", \"phone_number\" : \"718-773-0128\", \"primary_address_line_1\" : \"150 Albany Avenue\", \"program1\" : \"Academy for Health Careers\", \"psal_sports_boys\" : \"Baseball, Basketball, Cross Country, Handball, Indoor Track, Outdoor Track, Soccer, Tennis\", \"psal_sports_girls\" : \"Basketball, Cross Country, Indoor Track, Outdoor Track\", \"school_10th_seats\" : \"1\", \"school_accessibility_description\" : \"1\", \"school_email\" : \"dmartin8@schools.nyc.gov\", \"school_name\" : \"Academy for Health Careers\", \"school_sports\" : \"Cheerleading\", \"seats101\" : \"Yes-10\", \"seats9ge1\" : \"90\", \"seats9swd1\" : \"18\", \"shared_space\" : \"Yes\", \"start_time\" : \"8:30am\", \"state_code\" : \"NY\", \"subway\" : \"3 to Kingston Ave; A to Utica Ave; C to Kingston-Throop\", \"total_students\" : \"362\", \"website\" : \"schools.nyc.gov/SchoolPortals/17/K751\", \"zip\" : \"11213\" }, { \"academicopportunities1\" : \"Pre-Calculus, English Literature, Journalism, Senior Seminar, Columbia Business Entrepreneurship Course, CUNY College Now at City College\", \"academicopportunities2\" : \"Harlem Scholars, Young Harlem, Eleventh Grade Kaplan SAT Prep (NY Cares), Tenth Grade Sophomore Skills Program (NY Cares)\", \"academicopportunities3\" : \"Independent Reading Program, iMentor for incoming ninth grade scholars, US FIRST Robotics Program, CUNY At Home in College Program\", \"academicopportunities4\" : \"FDA II Expanded Learning Opportunities: Tribeca Film Institute, Music Production, Peer Tutoring, Digital Media\", \"academicopportunities5\" : \"Dance Theater of Harlem, Bridging Transitions\", \"addtl_info1\" : \"College Trips; Community Service Expected; Extended Day Program; Online Grading System; Summer Bridge Program; Uniform\", \"admissionspriority11\" : \"Priority to Districts 3 and 5 students or residents\", \"admissionspriority21\" : \"Then to New York City residents\", \"advancedplacement_courses\" : \"AP English, AP Environmental Science, AP Statistics, AP US History\", \"attendance_rate\" : \"0.800000012\", \"bbl\" : \"1018300019\", \"bin\" : \"1055204\", \"boro\" : \"M\", \"borough\" : \"MANHATTAN\", \"building_code\" : \"M088\", \"bus\" : \"M1, M10, M102, M11, M116, M2, M3, M4, M7\", \"census_tract\" : \"218\", \"city\" : \"Manhattan\", \"code1\" : \"M39A\", \"code2\" : \"M39B\", \"college_career_rate\" : \"0.524999976\", \"community_board\" : \"10\", \"council_district\" : \"9\", \"dbn\" : \"03M860\", \"directions1\" : \"Please contact the school about the on-site requirement.\", \"eligibility2\" : \"Open only to continuing 8th graders\", \"ell_programs\" : \"English as a New Language\", \"end_time\" : \"3pm\", \"extracurricular_activities\" : \"Student Council, I-Mentor, Robotics, Young Harlem, Peer Tutoring, East Harlem Pride Athletics,Digital Media, Music Production, Film-Making, Sister to Sister, Band, Chorus, Painting Club, Video Game Club, Book Club, Fashion Club, Youth Historians Club\", \"fax_number\" : \"212-865-9281\", \"finalgrades\" : \"6-12\", \"grade9geapplicants1\" : \"387\", \"grade9geapplicants2\" : \"N/A\", \"grade9geapplicantsperseat1\" : \"5\", \"grade9geapplicantsperseat2\" : \"N/A\", \"grade9gefilledflag1\" : \"N\", \"grade9gefilledflag2\" : \"N/A\", \"grade9swdapplicants1\" : \"150\", \"grade9swdapplicants2\" : \"N/A\", \"grade9swdapplicantsperseat1\" : \"9\", \"grade9swdapplicantsperseat2\" : \"N/A\", \"grade9swdfilledflag1\" : \"Y\", \"grade9swdfilledflag2\" : \"N/A\", \"grades2018\" : \"6-12\", \"graduation_rate\" : \"0.638000011\", \"interest1\" : \"Science & Math\", \"interest2\" : \"Science & Math\", \"language_classes\" : \"Spanish\", \"latitude\" : \"40.80261\", \"location\" : \"215 West 114th Street, Manhattan NY 10026 (40.80261, -73.954152)\", \"longitude\" : \"-73.9542\", \"method1\" : \"Ed. Opt.\", \"method2\" : \"For Continuing 8th Graders\", \"neighborhood\" : \"Central Harlem South\", \"nta\" : \"Central Harlem South                                                       \", \"offer_rate1\" : \"Â—29% of offers went to this group\", \"overview_paragraph\" : \"Frederick Douglass Academy II is a college preparatory secondary school focused on creating the next generation of societyÂ’s leaders, who are committed to excellence in character, scholastic achievement, and community service through a rigorous academic curriculum that exceeds national standards. We are a nurturing and supportive community of learners that provides Scholars an extraordinary opportunity for personal growth.\", \"pct_stu_enough_variety\" : \"0.800000012\", \"pct_stu_safe\" : \"0.769999981\", \"phone_number\" : \"212-865-9260\", \"prgdesc1\" : \"The Douglass STEM Institute prepares Scholars for success in colleges and careers focused on Science, Technology, Engineering and Mathematics (STEM) through a performance task based curriculum. In addition, to our core Science and Math sequence, Scholars have access to Advanced Placement courses as well as electives focused on Robotics and Engineering. Scholars will be equipped with a foundation of essential knowledge and skills to critically explore and solve real world problems.\", \"prgdesc2\" : \"The Douglass STEM Institute prepares Scholars for success in colleges and careers focused on Science, Technology, Engineering and Mathematics (STEM) through a performance task based curriculum. In addition, to our core Science and Math sequence, Scholars have access to Advanced Placement courses as well as electives focused on Robotics and Engineering. Scholars will be equipped with a foundation of essential knowledge and skills to critically explore and solve real world problems.\", \"primary_address_line_1\" : \"215 West 114th Street\", \"program1\" : \"The Douglass STEM Institute\", \"program2\" : \"The Douglass STEM Institute\", \"psal_sports_boys\" : \"Baseball, Basketball, Bowling, Football, Outdoor Track, Soccer, Volleyball, Wrestling\", \"psal_sports_girls\" : \"Basketball, Flag Football, Golf, Outdoor Track, Soccer, Softball, Volleyball\", \"requirement1_1\" : \"Group interview\", \"requirement2_1\" : \"Writing Exercise\", \"school_10th_seats\" : \"1\", \"school_accessibility_description\" : \"1\", \"school_email\" : \"enroll@fda2.org\", \"school_name\" : \"Frederick Douglass Academy II Secondary School\", \"school_sports\" : \"Boys: Football, Cross Country, Baseball, Outdoor Track; Girls: Flag Football, Golf, Soccer, Outdoor Track, Volleyball\", \"seats101\" : \"No\", \"seats102\" : \"Yes\", \"seats9ge1\" : \"80\", \"seats9ge2\" : \"N/A\", \"seats9swd1\" : \"16\", \"seats9swd2\" : \"N/A\", \"shared_space\" : \"Yes\", \"start_time\" : \"8am\", \"state_code\" : \"NY\", \"subway\" : \"2, 3, B, C to 116th St\", \"total_students\" : \"390\", \"website\" : \"www.fda2.org\", \"zip\" : \"10026\" }]".data(using: .utf8)!
            
        case .getSchoolScores:
            return "[{ \"dbn\" : \"17K751\", \"num_of_sat_test_takers\" : \"s\", \"sat_critical_reading_avg_score\" : \"s\", \"sat_math_avg_score\" : \"s\", \"sat_writing_avg_score\" : \"s\", \"school_name\" : \"ACADEMY FOR HEALTH CAREERS\" }, { \"dbn\" : \"03M860\", \"num_of_sat_test_takers\" : \"43\", \"sat_critical_reading_avg_score\" : \"356\", \"sat_math_avg_score\" : \"379\", \"sat_writing_avg_score\" : \"361\", \"school_name\" : \"FREDERICK DOUGLASS ACADEMY II SECONDARY SCHOOL\" }]".data(using: .utf8)!
            
        }
    }
    
    
    var headers: [String:String]? {
        
        let obj =  [
            "content-type" : "application/json",
            "X-App-Token" : appToken

            ] as [String : String]
        
        return obj
    }
    
    var parameters: [String: Any]? {
        
        switch self {
            case .getSchools,
                 .getSchoolScores:
                return nil
        }
    }
    
    var path: String {
        switch self {
        case .getSchools:
            return "resource/97mf-9njv.json"
            
        case .getSchoolScores:
            return "resource/734v-jeq5.json"
            
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var task: Task {
        switch self {
            
        case .getSchools:
            return .requestPlain
            
        case .getSchoolScores:
            return .requestPlain
            
        }
    }
}
