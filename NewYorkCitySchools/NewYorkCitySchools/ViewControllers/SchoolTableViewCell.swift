//
//  SchoolTableViewCell.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolNameLbl:UILabel!
    @IBOutlet weak var boroughNameLbl:UILabel!

    var school:School!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
